using Microsoft.AspNetCore.Antiforgery;
using ShellBeckMPA.Controllers;

namespace ShellBeckMPA.Web.Host.Controllers
{
    public class AntiForgeryController : ShellBeckMPAControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
