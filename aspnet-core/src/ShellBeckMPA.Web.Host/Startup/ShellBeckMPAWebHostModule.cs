﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ShellBeckMPA.Configuration;

namespace ShellBeckMPA.Web.Host.Startup
{
    [DependsOn(
       typeof(ShellBeckMPAWebCoreModule))]
    public class ShellBeckMPAWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ShellBeckMPAWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ShellBeckMPAWebHostModule).GetAssembly());
        }
    }
}
