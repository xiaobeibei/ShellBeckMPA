﻿using Abp.MultiTenancy;
using ShellBeckMPA.Authorization.Users;

namespace ShellBeckMPA.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
