﻿using Abp.Authorization;
using ShellBeckMPA.Authorization.Roles;
using ShellBeckMPA.Authorization.Users;

namespace ShellBeckMPA.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
