﻿namespace ShellBeckMPA
{
    public class ShellBeckMPAConsts
    {
        public const string LocalizationSourceName = "ShellBeckMPA";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
