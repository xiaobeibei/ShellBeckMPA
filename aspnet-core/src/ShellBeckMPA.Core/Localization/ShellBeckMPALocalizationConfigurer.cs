﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ShellBeckMPA.Localization
{
    public static class ShellBeckMPALocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ShellBeckMPAConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ShellBeckMPALocalizationConfigurer).GetAssembly(),
                        "ShellBeckMPA.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
