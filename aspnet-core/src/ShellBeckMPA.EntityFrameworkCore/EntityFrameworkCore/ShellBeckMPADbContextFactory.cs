﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ShellBeckMPA.Configuration;
using ShellBeckMPA.Web;

namespace ShellBeckMPA.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ShellBeckMPADbContextFactory : IDesignTimeDbContextFactory<ShellBeckMPADbContext>
    {
        public ShellBeckMPADbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ShellBeckMPADbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ShellBeckMPADbContextConfigurer.Configure(builder, configuration.GetConnectionString(ShellBeckMPAConsts.ConnectionStringName));

            return new ShellBeckMPADbContext(builder.Options);
        }
    }
}
