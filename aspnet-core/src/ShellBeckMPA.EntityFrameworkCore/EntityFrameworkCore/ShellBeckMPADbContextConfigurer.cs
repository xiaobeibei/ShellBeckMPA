using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ShellBeckMPA.EntityFrameworkCore
{
    public static class ShellBeckMPADbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ShellBeckMPADbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ShellBeckMPADbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseMySql(connection);
        }
    }
}
