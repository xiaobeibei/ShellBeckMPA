﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ShellBeckMPA.Authorization.Roles;
using ShellBeckMPA.Authorization.Users;
using ShellBeckMPA.MultiTenancy;

namespace ShellBeckMPA.EntityFrameworkCore
{
    public class ShellBeckMPADbContext : AbpZeroDbContext<Tenant, Role, User, ShellBeckMPADbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public ShellBeckMPADbContext(DbContextOptions<ShellBeckMPADbContext> options)
            : base(options)
        {
        }
    }
}
