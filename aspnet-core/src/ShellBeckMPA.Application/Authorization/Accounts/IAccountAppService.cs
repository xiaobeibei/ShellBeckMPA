﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ShellBeckMPA.Authorization.Accounts.Dto;

namespace ShellBeckMPA.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
