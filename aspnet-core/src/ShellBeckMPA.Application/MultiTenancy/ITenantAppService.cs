﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ShellBeckMPA.MultiTenancy.Dto;

namespace ShellBeckMPA.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
