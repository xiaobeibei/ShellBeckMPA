using System.ComponentModel.DataAnnotations;

namespace ShellBeckMPA.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}