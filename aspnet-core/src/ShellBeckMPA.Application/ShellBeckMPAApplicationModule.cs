﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ShellBeckMPA.Authorization;

namespace ShellBeckMPA
{
    [DependsOn(
        typeof(ShellBeckMPACoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ShellBeckMPAApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ShellBeckMPAAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ShellBeckMPAApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
