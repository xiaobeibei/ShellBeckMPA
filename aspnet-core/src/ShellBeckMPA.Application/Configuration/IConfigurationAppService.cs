﻿using System.Threading.Tasks;
using ShellBeckMPA.Configuration.Dto;

namespace ShellBeckMPA.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
