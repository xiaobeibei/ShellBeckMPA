﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ShellBeckMPA.Configuration.Dto;

namespace ShellBeckMPA.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ShellBeckMPAAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
