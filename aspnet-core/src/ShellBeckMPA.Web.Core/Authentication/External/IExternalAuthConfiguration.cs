﻿using System.Collections.Generic;

namespace ShellBeckMPA.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
