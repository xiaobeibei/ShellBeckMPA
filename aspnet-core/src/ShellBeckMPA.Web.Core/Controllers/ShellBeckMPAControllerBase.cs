using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ShellBeckMPA.Controllers
{
    public abstract class ShellBeckMPAControllerBase: AbpController
    {
        protected ShellBeckMPAControllerBase()
        {
            LocalizationSourceName = ShellBeckMPAConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
