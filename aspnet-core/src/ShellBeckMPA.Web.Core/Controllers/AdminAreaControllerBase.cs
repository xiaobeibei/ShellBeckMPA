using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ShellBeckMPA.Controllers
{
    [Area("Admin")]
    public abstract class AdminAreaControllerBase : ShellBeckMPAControllerBase
    {
        protected AdminAreaControllerBase()
        {
        }
    }
}
