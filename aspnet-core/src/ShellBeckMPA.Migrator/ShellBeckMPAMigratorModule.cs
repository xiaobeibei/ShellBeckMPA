using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ShellBeckMPA.Configuration;
using ShellBeckMPA.EntityFrameworkCore;
using ShellBeckMPA.Migrator.DependencyInjection;

namespace ShellBeckMPA.Migrator
{
    [DependsOn(typeof(ShellBeckMPAEntityFrameworkModule))]
    public class ShellBeckMPAMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ShellBeckMPAMigratorModule(ShellBeckMPAEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(ShellBeckMPAMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                ShellBeckMPAConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ShellBeckMPAMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
