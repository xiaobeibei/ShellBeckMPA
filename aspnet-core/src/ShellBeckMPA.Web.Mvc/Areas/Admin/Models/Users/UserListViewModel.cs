using System.Collections.Generic;
using ShellBeckMPA.Roles.Dto;
using ShellBeckMPA.Users.Dto;

namespace ShellBeckMPA.Web.Areas.Admin.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
