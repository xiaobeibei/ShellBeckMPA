﻿using System.Collections.Generic;
using ShellBeckMPA.Roles.Dto;

namespace ShellBeckMPA.Web.Areas.Admin.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
