using Abp.AutoMapper;
using ShellBeckMPA.Sessions.Dto;

namespace ShellBeckMPA.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
