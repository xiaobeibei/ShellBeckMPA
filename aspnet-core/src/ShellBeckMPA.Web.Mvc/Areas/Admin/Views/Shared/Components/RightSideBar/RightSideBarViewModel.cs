﻿using ShellBeckMPA.Configuration.Ui;

namespace ShellBeckMPA.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
