﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ShellBeckMPA.Controllers;

namespace ShellBeckMPA.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : AdminAreaControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
