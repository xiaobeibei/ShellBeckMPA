﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ShellBeckMPA.Configuration;
using ShellBeckMPA.Web.Areas.Admin.Startup;

namespace ShellBeckMPA.Web.Startup
{
    [DependsOn(typeof(ShellBeckMPAWebCoreModule))]
    public class ShellBeckMPAWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ShellBeckMPAWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<ShellBeckMPANavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ShellBeckMPAWebMvcModule).GetAssembly());
        }
    }
}
