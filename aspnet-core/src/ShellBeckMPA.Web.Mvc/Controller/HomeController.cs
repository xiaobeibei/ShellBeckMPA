﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ShellBeckMPA.Controllers;

namespace ShellBeckMPA.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : ShellBeckMPAControllerBase
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home", new { area = "Admin" });
        }
    }
}
