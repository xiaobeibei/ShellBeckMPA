﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ShellBeckMPA.Web.Views
{
    public abstract class ShellBeckMPAViewComponent : AbpViewComponent
    {
        protected ShellBeckMPAViewComponent()
        {
            LocalizationSourceName = ShellBeckMPAConsts.LocalizationSourceName;
        }
    }
}
