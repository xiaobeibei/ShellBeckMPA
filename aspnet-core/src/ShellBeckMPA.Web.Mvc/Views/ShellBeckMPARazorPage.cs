﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace ShellBeckMPA.Web.Views
{
    public abstract class ShellBeckMPARazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected ShellBeckMPARazorPage()
        {
            LocalizationSourceName = ShellBeckMPAConsts.LocalizationSourceName;
        }
    }
}
